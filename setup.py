from distutils.core import setup

setup(
    name='test-flask',
    version='1.0',
    packages=[''],
    url='https://bitbucket.org/phistrom/test-flask',
    license='Apache 2.0',
    author='Phillip Stromberg',
    author_email='phillip+git@stromberg.me',
    description='A goofy little test for pip',
    install_requires=[
        'Flask',
    ]
)
